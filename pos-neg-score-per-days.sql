select 
	DATE("date") days, 
	sum(case when score >= 0 then 1 else 0 end) as num_pos_scores,
	sum(case when score < 0 then 1 else 0 end) as num_neg_scores
from public.assessments a
where date between '2011-03-01' and '2011-04-30'
group by days