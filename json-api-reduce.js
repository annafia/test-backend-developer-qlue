function reduceJsonApi(data) {
    let result = { h: [], d: [] }

    if (data.length > 0) {
        for (let attr in data[0]) {
            result.h.push(attr)
        }

        for (let row of data) {
            let minifiedData = []
            for (let attr of result.h) {
                minifiedData.push(row[attr])
            }
            result.d.push(minifiedData)
        }
    }

    return result
}

let data = [
    {"username":"ali","hair_color":"brown","height":1.2},
    {"username":"marc","hair_color":"blue","height":1.4},
    {"username":"joe","hair_color":"brown","height":1.7},
    {"username":"zehua","hair_color":"black","height":1.8}
]

let res = reduceJsonApi(data)
console.log(res)