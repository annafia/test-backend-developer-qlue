for file in /tmp/migg33/inner.folder/**; do 
    dirname="${file%/*}/"
    basename="${file:${#dirname}}"
    filename="${basename%.*}"
    ext="${basename:${#filename}}"
    if [[ $ext == ".txt" ]]; then
        mv "$file" "$dirname${filename}.dat"
    fi
done