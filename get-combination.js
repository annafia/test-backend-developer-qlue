function getCombinations(num) {
    let results = []
    function getCombinationRecursive(sets, index, num, previousNum) {
        if (previousNum < 0) return
    
        if (previousNum == 0) {
            let result = []
            for (let i = 0; i < index; i++)
                result.push(sets[i])
            results.push(result)
        }
    
        let prev = index == 0 ? 1 : sets[index - 1]
    
        for (let k = prev; k <= num; k++) {
            sets[index] = k
            getCombinationRecursive(sets, index + 1, num, previousNum - k)
        }
    }

    let setsNumber = [num]
    getCombinationRecursive(setsNumber, 0, num, num)

    return results
}